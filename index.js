
//WDC028-26 Node.js Introduction
let http = require("http");

http.createServer(function(req,res){

	//
	if(req.url === "/") {
			res.writeHead(200,{'Content-Type': 'text/plain'});
			res.end("Welcome to B176 E-Commerce.");
	} else if (req.url === "/products") {
			res.writeHead(200,{'Content-Type': 'text/plain'});
			res.end("Welcome to the Products Page. Here is the list of our products.");
	} else if (req.url === "/orders") {
			res.writeHead(200,{'Content-Type': 'text/plain'});
			res.end("Welcome to your orders. View your orders here.");
	} else {
			res.writeHead(404, {'Content-Type':'text/plain'});
			res.end("Resource not found");
	}


}).listen(5000);

console.log("Server is running on localhost:5000")


